# enolib

An eno parsing library.

## Installation

TODO: Not actually published

```bash
composer require eno-lang/enolib
```

## Getting started

A minimal example:

```php
use Enolib\Parser;

$document = Parser::parse('Message: Hello World');

echo( $document->field('Greeting')->requiredStringValue() );  // prints 'Hello World!'
```

## Complete documentation and API reference

See [simonrepp.com/enolib-php](https://simonrepp.com/enolib-php/)

## Generate documentation

As a prerequisite you need to install [phpdoc](https://www.phpdoc.org/).

Generate user documentation (excludes internals of the implementation):

```bash
phpdoc
```

Generate developer documentation (includes internals of the implementation):

```bash
phpdoc --sourcecode --visibility=internal
```

Configuration options can be changed in `phpdoc.dist.xml`, see [here](https://docs.phpdoc.org/3.0/guide/references/configuration.html) for instructions.
