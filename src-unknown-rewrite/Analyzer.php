<?php declare(strict_types=1);

namespace Enolib;
use \stdClass;
use Enolib\{Error, LineRegex};

/**
 * Main parsing module - reads the document line-by-line, creates the AST, leaves copies unresolved.
 * 
 * @internal
 */
class Analyzer {
    private stdClass $context;
    private int $index = 0;
    private ?stdClass $instruction;
    private ?stdClass $last_comment = null;
    private ?stdClass $last_continuable_instruction = null;
    private ?stdClass $last_field = null;
    private int $line = 0;
    private ?array $match = null;
    
    function __construct(stdClass $context) {
        $this->context = $context;

        $this->context->document_instruction = (object) [
            'depth' => 0,
            'index' => 0,
            'length' => 0,
            'line' => 0,
            'ranges' => [
                'section_operator' => [0, 0],
                'key' => [0, 0]
            ],
            'subinstructions' => []
        ];

        $this->context->instructions = [];
        $this->context->non_section_template_index = [];
        $this->context->section_template_index = [];
        $this->context->unresolved_non_section_instructions = [];
        $this->context->unresolved_section_instructions = [];
        
        $this->active_section_instructions = [$this->context->document_instruction];
    }
    
    private function attachLastCommentToElement() {
        if($this->last_comment_instruction !== null) {
            $this->instruction->associated_comment_instruction = $this->last_comment_instruction;
            $this->last_comment_instruction = null;
        }
    }
  
    private function attachLastCommentToDocument() : void {
        // if ($this->last_comment !== null) {
        //     if ($this->context->last_section_level_instruction_stack[$this->context->section_level]->id & INHERENT_IS_DOCUMENT &&
        //         $this->context->last_comment->line_number === 1) {
        //         assemble_comment(&$this->context->instance->assembly, $this->context->last_comment);
        //         ((struct Document *)$this->context->last_section_level_instruction_stack[$this->context->section_level])->associated_comment = $this->context->last_comment;
        //         $this->context->last_section_level_instruction_stack[$this->context->section_level]->id |= DERIVED_HAS_COMMENT;
        //     } else {
        //         *$this->context->last_next_unassociated_comment_pointer = $this->context->last_comment;
        //         $this->context->last_next_unassociated_comment_pointer = (struct Comment **)&$this->context->last_comment_or_continuation->next_sibling;
        //     }
        // 
        //     $this->last_comment = null;
        // }
    }

    private function tokenizeErrorContext(stdClass $context) : stdClass {
        $first_instruction = null;

        while(true) {
            $end_of_line_column = strpos($context->input, "\n", $this->index);

            if($end_of_line_column === false) {
                $instruction = (object) [
                    'index' => $this->index,
                    'length' => strlen($context->input) - $this->index,
                    'line' => $this->line
                ];

                $context->instructions[] = $instruction;

                if($first_instruction === null) {
                    $first_instruction = $instruction;
                }

                return $first_instruction;
            } else {
                $instruction = (object) [
                    'index' => $this->index,
                    'length' => $end_of_line_column - $this->index,
                    'line' => $this->line
                ];

                $context->instructions[] = $instruction;

                if($first_instruction === null) {
                    $first_instruction = $instruction;
                }

                $this->index = $end_of_line_column + 1;
                $this->line++;
            }
        }
    }

    function analyze() : void {
        $embed = false;
        
        while(true) {
            $matched = preg_match(
                LineRegex::LINE,
                $this->context->input,
                $this->match,
                PREG_OFFSET_CAPTURE | PREG_UNMATCHED_AS_NULL,
                $this->index
            );

            if($matched != 1 || $this->match[0][1] != $this->index) {
                $this->instruction = $this->tokenizeErrorContext($this->context, $this->index, $this->line);
                throw Error::invalidLine($this->context, $this->instruction);
            }

            $this->instruction = (object) [
                'index' => $this->index,
                'line' => $this->line++
            ];

            if(isset($this->match[LineRegex::EMPTY_LINE_INDEX][0])) {
                $this->attachLastCommentToDocument();
            } else if(isset($this->match[LineRegex::FIELD_OPERATOR_INDEX][0])) {
                $this->readField();
            } else if(isset($this->match[LineRegex::ITEM_OPERATOR_INDEX][0])) {
                $this->readItem();
            } else if(isset($this->match[LineRegex::ENTRY_OPERATOR_INDEX][0])) {
                $this->readEntry();
            } else if(isset($this->match[LineRegex::CONTINUATION_OPERATOR_INDEX][0])) {
                $this->readContinuation();
            } else if(isset($this->match[LineRegex::SECTION_OPERATOR_INDEX][0])) {
                $this->readSection();
            } else if(isset($this->match[LineRegex::EMBED_OPERATOR_INDEX][0])) {
                $embed = true;
                $this->readEmbed();
            } else if(isset($this->match[LineRegex::TEMPLATE_INDEX][0])) {
                $this->readCopy();
            } else if(isset($this->match[LineRegex::COMMENT_OPERATOR_INDEX][0])) {
                $this->readComment();
            }

            if($embed) {
                $embed = false;
            } else {
                $this->instruction->length = $this->match[0][1] + strlen($this->match[0][0]) - $this->index;
                $this->index += $this->instruction->length + 1;
                $this->context->instructions[] = $this->instruction;
            }

            if($this->index >= strlen($this->context->input)) {
                $this->attachLastCommentToDocument();
                
                if (strlen($this->context->input) > 0 &&
                    $this->context->input[strlen($this->context->input) - 1] === "\n") {
                    $this->context->input_linecount = $this->line + 1; // TODO: Verify we have no one-off issue here
                } else {
                    $this->context->input_linecount = $this->line; // TODO: Verify we have no one-off issue here
                }

                break;
            }
        }
    }
    
    private function readComment() : void {
        $operator_column = $this->match[LineRegex::COMMENT_OPERATOR_INDEX][1] - $this->index;
        $value_column = $this->match[LineRegex::COMMENT_VALUE_INDEX][1] - $this->index;

        $this->instruction->comment = $this->match[LineRegex::COMMENT_VALUE_INDEX][0];
        $this->instruction->ranges = [
            'comment' => [$value_column, $value_column + strlen($this->instruction->comment)],
            'comment_operator' => [$operator_column, $operator_column + 1]
        ];
        $this->instruction->type = 'COMMENT';
        
        $this->last_comment_instructions[] = $this->instruction;
    }
    
    private function readContinuation() : void {
        if($this->last_continuable_instruction === null) {
          if($this->last_field_instruction && array_key_exists('template', $this->last_field_instruction)){
            throw Parsing::lineContinuationOnCopiedElement($this->context, $instruction, $this->last_field_instruction);
          } else {
            throw Parsing::missingElementForContinuation($this->context, $instruction);
          }
        }
        
        $this->last_unassigned_comment_instruction_index = null;
        $this->attachLastCommentToElement();
        
        $operator_column = $this->match[LineRegex::CONTINUATION_OPERATOR_INDEX][1] - $this->index;

        $this->instruction->ranges = [ 'continuation_operator' => [$operator_column, $operator_column + 1] ];
        $this->instruction->spaced = ($this->match[LineRegex::CONTINUATION_OPERATOR_INDEX][0] === '\\');
        $this->instruction->type = 'CONTINUATION';

        if(isset($this->match[LineRegex::CONTINUATION_VALUE_INDEX][0])) {
            $value = $this->match[LineRegex::CONTINUATION_VALUE_INDEX][0];
            $value_column = $this->match[LineRegex::CONTINUATION_VALUE_INDEX][1] - $this->index;

            $this->instruction->value = $value;
            $this->instruction->ranges['value'] = [$value_column, $value_column + strlen($value)];
        } else {
            $this->instruction->value = null;
        }
        
        if($this->last_field_instruction->type === 'ELEMENT') {
          $this->last_field_instruction->type = 'FIELD';
          $this->last_field_instruction->value = null;
        }
        
        $this->last_continuable_instruction->subinstructions[] = $instruction;
    }
    
    private function readCopy() : void {
        $template = $this->match[LineRegex::TEMPLATE_INDEX][0];
        $copy_operator = $this->match[LineRegex::COPY_OPERATOR_INDEX][0];
        $copy_operator_column = $this->match[LineRegex::COPY_OPERATOR_INDEX][1] - $this->index;

        if(isset($this->match[LineRegex::KEY_UNESCAPED_INDEX][0])) {
            $key = $this->match[LineRegex::KEY_UNESCAPED_INDEX][0];

            $key_column = $this->match[LineRegex::KEY_UNESCAPED_INDEX][1] - $this->index;

            $this->instruction->key = $key;
            $this->instruction->ranges = [
                'copy_operator' => [$copy_operator_column, $copy_operator_column + strlen($copy_operator)],
                'key' => [$key_column, $key_column + strlen($this->instruction->key)]
            ];
        } else {
            $key = $this->match[LineRegex::KEY_ESCAPED_INDEX][0];

            $escape_operator = $this->match[LineRegex::KEY_ESCAPE_BEGIN_OPERATOR_INDEX][0];
            $escape_begin_operator_column = $this->match[LineRegex::KEY_ESCAPE_BEGIN_OPERATOR_INDEX][1] - $this->index;
            $key_column = $this->match[LineRegex::KEY_ESCAPED_INDEX][1] - $this->index;
            $escape_end_operator_column = $this->match[LineRegex::KEY_ESCAPE_END_OPERATOR_INDEX][1] - $this->index;

            $this->instruction->key = $key;
            $this->instruction->ranges = [
                'copy_operator' => [$copy_operator_column, $copy_operator_column + strlen($copy_operator)],
                'escape_begin_operator' => [$escape_begin_operator_column, $escape_begin_operator_column + strlen($escape_operator)],
                'escape_end_operator' => [$escape_end_operator_column, $escape_end_operator_column + strlen($escape_operator)],
                'key' => [$key_column, $key_column + strlen($key)]
            ];
        }

        $this->instruction->template = $template;
        $this->instruction->type = 'NON_SECTION_ELEMENT_COPY';
        $this->last_continuable_instruction = null;

        $template_column = $this->match[LineRegex::TEMPLATE_INDEX][1] - $this->index;
        $this->instruction->ranges['template'] = [$template_column, $template_column + strlen($template)];
        
        if (property_exists($this->context->non_section_copy_ops_by_key, $template)) {
            $this->context->non_section_copy_ops_by_key[$template]->targets[] = $this->instruction;
        } else {
            $this->context->non_section_copy_ops_by_key[$template] = (object) [
                'targets' => [$this->instruction]
            ];
        }
        
        $this->last_section_instruction->subinstructions[] = $instruction;
    }
    
    private function readEmbed() : void {
        $this->attachLastCommentToElement();
        
        $this->last_continuable_instruction = null;
        $this->last_field_instruction = $this->instruction;
        
        $operator = $this->match[LineRegex::EMBED_OPERATOR_INDEX][0];
        $key = $this->match[LineRegex::EMBED_KEY_INDEX][0];
        $this->instruction->key = $key;
        $this->instruction->type = 'EMBED_BEGIN';

        $operator_column = $this->match[LineRegex::EMBED_OPERATOR_INDEX][1] - $this->index;
        $key_column = $this->match[LineRegex::EMBED_KEY_INDEX][1] - $this->index;
        $this->instruction->length = strlen($this->match[0][0]);
        $this->instruction->ranges = [
            'embed_operator' => [$operator_column, $operator_column + strlen($operator)],
            'key' => [$key_column, $key_column + strlen($key)]
        ];

        $this->index = $this->index + $this->instruction->length + 1;

        $this->context->instructions[] = $this->instruction;

        $start_of_embed_column = $this->index;

        $key_escaped = preg_quote($this->instruction->key);
        $terminator_regex = "/[^\\S\\n]*(${operator})(?!-)[^\\S\\n]*(${key_escaped})[^\\S\\n]*(?=\\n|$)/A";

        while(true) {
            $matched = preg_match(
                $terminator_regex,
                $this->context->input,
                $terminator_match,
                PREG_OFFSET_CAPTURE | PREG_UNMATCHED_AS_NULL,
                $this->index
            );

            if($matched === 1) {
                if($this->line > $this->instruction->line + 1) {
                    $this->instruction->value_range = [$start_of_embed_column, $this->index - 2];
                }

                $operator_column = $terminator_match[1][1] - $this->index;
                $key_column = $terminator_match[2][1] - $this->index;

                $this->instruction['terminator_index'] = $this->index;
                $this->instruction['terminator_length'] = strlen($terminator_match[0][0]);
                $this->instruction['terminator_line'] = $this->line;
                $this->instruction['ranges']['terminator_key'] = [$operator_column, $operator_column + strlen($operator)];
                $this->instruction['ranges']['terminator_operator'] = [$key_column, $key_column + strlen($key)];

                // $this->context->instructions[] = $this->instruction;
                // $this->index = $this->index + $this->instruction->length + 1;
                // $this->line++;

                break;
            } else {
                $end_of_line_column = strpos($this->context->input, "\n", $this->index);

                if($end_of_line_column === false) {
                    $this->context->instructions[] = (object) [
                        'index' => $this->index,
                        'length' => strlen($this->context->input) - $this->index,
                        'line' => $this->line
                    ];

                    throw Error::unterminatedMultilineField($this->context, $this->instruction);
                } else {
                    // TODO: We don't allocate every value line anymore! (everything goes onto a single instruction!)
                    // $this->context->instructions[] = (object) [
                    //     'index' => $this->index,
                    //     'length' => $end_of_line_column - $this->index,
                    //     'line' => $this->line,
                    //     'ranges' => [ 'content' => [0, $end_of_line_column - $this->index] ],
                    //     'type' => 'EMBED_VALUE'
                    // ];

                    $this->index = $end_of_line_column + 1;
                    $this->line++;
                }
            }
        }
        
        $this->last_section_instruction->subinstructions[] = $this->instruction;
    }
    
    private function readEntry() : void {
        if($this->last_field_instruction === null) {
          throw Error::missingFieldForEntry($this->context, $this-$this->instruction);
        }
        
        $this->attachLastCommentToElement();
        
        $this->instruction->subinstructions = [];
        $this->instruction->type = 'ENTRY';
        
        $entry_operator_column = $this->match[LineRegex::ENTRY_OPERATOR_INDEX][1] - $this->index;

        if(isset($this->match[LineRegex::KEY_UNESCAPED_INDEX][0])) {
            $key = $this->match[LineRegex::KEY_UNESCAPED_INDEX][0];
            $key_column = $this->match[LineRegex::KEY_UNESCAPED_INDEX][1] - $this->index;

            $this->instruction->key = $key;
            $this->instruction->ranges = [
                'entry_operator' => [$entry_operator_column, $entry_operator_column + 1],
                'key' => [$key_column, $key_column + strlen($key)]
            ];
        } else {
            $escape_operator = $this->match[LineRegex::KEY_ESCAPE_BEGIN_OPERATOR_INDEX][0];
            $key = $this->match[LineRegex::KEY_ESCAPED_INDEX][0];
            $escape_begin_operator_column = $this->match[LineRegex::KEY_ESCAPE_BEGIN_OPERATOR_INDEX][1] - $this->index;
            $key_column = $this->match[LineRegex::KEY_ESCAPED_INDEX][1] - $this->index;
            $escape_end_operator_column = $this->match[LineRegex::KEY_ESCAPE_END_OPERATOR_INDEX][1] - $this->index;

            $this->instruction->key = $key;
            $this->instruction->ranges = [
                'escape_begin_operator' => [$escape_begin_operator_column, $escape_begin_operator_column + strlen($escape_operator)],
                'escape_end_operator' => [$escape_end_operator_column, $escape_end_operator_column + strlen($escape_operator)],
                'entry_operator' => [$entry_operator_column, $entry_operator_column + 1],
                'key' => [$key_column, $key_column + strlen($key)]
            ];
        }

        if(isset($this->match[LineRegex::ENTRY_VALUE_INDEX][0])) {
            $value = $this->match[LineRegex::ENTRY_VALUE_INDEX][0];
            $value_column = $this->match[LineRegex::ENTRY_VALUE_INDEX][1] - $this->index;
            $this->instruction->ranges['value'] = [$value_column, $value_column + strlen($value)];
            $this->instruction->value = $value;
        } else {
            $this->instruction->value = null;
        }
        
        if($this->last_field_instruction->type === 'FIELDSET') {
          $this->last_field_instruction->subinstructions[] = $this->instruction;
          $this->last_continuable_instruction = $this->instruction;
          return;
        }

        if($this->last_field_instruction->type === 'ELEMENT') {
          $this->last_field_instruction->type = 'FIELDSET';
          $this->last_field_instruction->subinstructions[] = $this->instruction;
          $this->last_continuable_instruction = $this->instruction;
          return;
        }

        throw Parsing::missingFieldsetForFieldsetEntry($this->context, $this->instruction);
    }
    
    private function readField() : void {
        $this->attachLastCommentToElement();

        $this->instruction->subinstructions = [];
        $this->instruction->type = 'FIELD';
        $this->last_continuable_instruction = $this->instruction;
        $this->last_field_instruction = $this->instruction;
        
        $field_operator_column = $this->match[LineRegex::FIELD_OPERATOR_INDEX][1] - $this->index;

        if(isset($this->match[LineRegex::KEY_UNESCAPED_INDEX][0])) {
            $key = $this->match[LineRegex::KEY_UNESCAPED_INDEX][0];
            $key_column = $this->match[LineRegex::KEY_UNESCAPED_INDEX][1] - $this->index;

            $this->instruction->key = $key;
            $this->instruction->ranges = [
                'element_operator' => [$field_operator_column, $field_operator_column + 1],
                'key' => [$key_column, $key_column + strlen($key)]
            ];
        } else {
            $key = $this->match[LineRegex::KEY_ESCAPED_INDEX][0];
            $escape_operator = $this->match[LineRegex::KEY_ESCAPE_BEGIN_OPERATOR_INDEX][0];
            $escape_begin_operator_column = $this->match[LineRegex::KEY_ESCAPE_BEGIN_OPERATOR_INDEX][1] - $this->index;
            $key_column = $this->match[LineRegex::KEY_ESCAPED_INDEX][1] - $this->index;
            $escape_end_operator_column = $this->match[LineRegex::KEY_ESCAPE_END_OPERATOR_INDEX][1] - $this->index;

            $this->instruction->key = $key;
            $this->instruction->ranges = [
                'escape_begin_operator' => [$escape_begin_operator_column, $escape_begin_operator_column + strlen($escape_operator)],
                'escape_end_operator' => [$escape_end_operator_column, $escape_end_operator_column + strlen($escape_operator)],
                'element_operator' => [$field_operator_column, $field_operator_column + 1],
                'key' => [$key_column, $key_column + strlen($key)]
            ];
        }

        if(isset($this->match[LineRegex::FIELD_VALUE_INDEX][0])) {
            $value = $this->match[LineRegex::FIELD_VALUE_INDEX][0];
            $this->instruction->value = $value;

            $value_column = $this->match[LineRegex::FIELD_VALUE_INDEX][1] - $this->index;
            $this->instruction->ranges['value'] = [$value_column, $value_column + strlen($value)];
        } else {
            $this->instruction->value = null;
        }
        
        $this->last_section_instruction->subinstructions[] = $instruction;
    }
    
    private function readItem() : void {
        if($this->last_field_instruction === null) { // TODO: && (has_items || !has_value)
          throw Error::missingListForListItem($this->context, $instruction);
        }
        
        $this->attachLastCommentToElement();
        
        $operator_column = $this->match[LineRegex::ITEM_OPERATOR_INDEX][1] - $this->index;

        $this->instruction->ranges = [ 'item_operator' => [$operator_column, $operator_column + 1] ];
        $this->instruction->subinstructions = [];
        $this->instruction->type = 'ITEM';

        if(isset($this->match[LineRegex::ITEM_VALUE_INDEX][0])) {
            $this->instruction->value = $this->match[LineRegex::ITEM_VALUE_INDEX][0];
            $value_column = $this->match[LineRegex::ITEM_VALUE_INDEX][1] - $this->index;
            $this->instruction->ranges['value'] = [$value_column, $value_column + strlen($this->instruction->value)];
        } else {
            $this->instruction->value = null;
        }
        
        if($this->last_field_instruction->type === 'LIST') {
          $this->last_field_instruction->subinstructions[] = $this->instruction;
          $this->last_continuable_instruction = $this->instruction;
          return;
        }

        if($this->last_field_instruction->type === 'ELEMENT') {
          $this->last_field_instruction->type = 'LIST';
          $this->last_field_instruction->subinstructions[] = $this->instruction;
          $this->last_continuable_instruction = $this->instruction;
          return;
        }

        throw Parsing::missingListForListItem($this->context, $this->instruction);
    }
    
    private function readSection() : void {
        $this->attachLastCommentToElement();
        
        $section_operator = $this->match[LineRegex::SECTION_OPERATOR_INDEX][0];

        $this->instruction->depth = strlen($section_operator);
        $this->instruction->subinstructions = [];
        $this->instruction->type = 'SECTION';
        
        if($this->instruction->depth - $this->last_section_instruction->depth > 1) {
          throw Error::sectionHierarchyLayerSkip($this->context, $instruction, $this->last_section_instruction);
        }

        $section_operator_column = $this->match[LineRegex::SECTION_OPERATOR_INDEX][1] - $this->index;
        $key_end_column = null;

        if(isset($this->match[LineRegex::SECTION_KEY_UNESCAPED_INDEX][0])) {
            $key = $this->match[LineRegex::SECTION_KEY_UNESCAPED_INDEX][0];

            $key_column = $this->match[LineRegex::SECTION_KEY_UNESCAPED_INDEX][1] - $this->index;
            $key_end_column = $key_column + strlen($key);

            $this->instruction->key = $key;
            $this->instruction->ranges = [
                'key' => [$key_column, $key_column + strlen($key)],
                'section_operator' => [$section_operator_column, $section_operator_column + strlen($section_operator)]
            ];
        } else {
            $key = $this->match[LineRegex::SECTION_KEY_ESCAPED_INDEX][0];

            $escape_operator = $this->match[LineRegex::SECTION_KEY_ESCAPE_BEGIN_OPERATOR_INDEX][0];
            $key = $this->match[LineRegex::SECTION_KEY_ESCAPED_INDEX][0];
            $escape_begin_operator_column = $this->match[LineRegex::SECTION_KEY_ESCAPE_BEGIN_OPERATOR_INDEX][1] - $this->index;
            $key_column = $this->match[LineRegex::SECTION_KEY_ESCAPED_INDEX][1] - $this->index;
            $escape_end_operator_column = $this->match[LineRegex::SECTION_KEY_ESCAPE_END_OPERATOR_INDEX][1] - $this->index;

            $this->instruction->key = $key;
            $this->instruction->ranges = [
                'escape_begin_operator' => [$escape_begin_operator_column, $escape_begin_operator_column + strlen($escape_operator)],
                'escape_end_operator' => [$escape_end_operator_column, $escape_end_operator_column + strlen($escape_operator)],
                'key' => [$key_column, $key_column + strlen($key)],
                'section_operator' => [$section_operator_column, $section_operator_column + strlen($section_operator)]
            ];
        }
        
        if ($this->instruction->depth > $this->last_section_instruction->depth) {
          $this->last_section_instruction->subinstructions[] = $this->instruction;
        } else {
          while ($this->active_section_instructions[count($this->active_section_instructions) - 1]->depth >= $this->instruction->depth) {
            array_pop($this->active_section_instructions);
          }

          $this->active_section_instructions[count($this->active_section_instructions) - 1]->subinstructions[] = $this->instruction;
        }
        
        $this->last_continuable_instruction = null;
        $this->last_field_instruction = null;
        $this->last_section_instruction = $instruction;
        $this->active_section_instructions[] = $instruction;

        if (isset($this->match[LineRegex::SECTION_TEMPLATE_INDEX][0])) {
            $template = $this->match[LineRegex::SECTION_TEMPLATE_INDEX][0];
            $this->instruction->template = $template;

            $copy_operator = $this->match[LineRegex::SECTION_COPY_OPERATOR_INDEX][0];
            $copy_operator_column = $this->match[LineRegex::SECTION_COPY_OPERATOR_INDEX][1] - $this->index;
            $template_column = $this->match[LineRegex::SECTION_TEMPLATE_INDEX][1] - $this->index;

            if($copy_operator === '<') {
                $this->instruction->deep_copy = false;
                $this->instruction->ranges['copy_operator'] = [$copy_operator_column, $copy_operator_column + strlen($copy_operator)];
            } else /* if ($copy_operator === '<<') */ {
                $this->instruction->deep_copy = true;
                $this->instruction->ranges['deep_copy_operator'] = [$copy_operator_column, $copy_operator_column + strlen($copy_operator)];
            }

            $this->instruction->ranges['template'] = [$template_column, $template_column + strlen($template)];
            
            if (property_exists($this->context->section_copy_ops_by_key, $template)) {
                $this->context->section_copy_ops_by_key[$template]->targets[] = $this->instruction;
            } else {
                $this->context->section_copy_ops_by_key[$template] = (object) [
                    'targets' => [$this->instruction]
                ];
            }
        }
    }
}
