<?php declare(strict_types=1);

namespace Enolib;

/**
 * Provides a regular expression that can dissect any valid line of Eno, used by the Parser.
 *
 * LineRegex defines a long, single regular expression that matches and dissects
 * into capture groups any valid line that can occur in an Eno document.
 * This regex rule is used by the Parser to analyze a document, line by line.
 * 
 * This class should be studied from the bottom up, where the rule in its entirety
 * is assembled. Everything above are the (continuously more granular) sub-rules
 * that make up the single regular expression in the end.
 *
 * @internal
 */
class LineRegex {
  private const OPTIONAL = '([^\\n]+?)?';
  private const REQUIRED = '(\\S[^\\n]*?)';

  //
  private const EMPTY_LINE = '()';
  public const EMPTY_LINE_INDEX = 1;

  // | value
  // \ value
  private const CONTINUATION = '(\\||\\\\)[^\\S\\n]*'.self::OPTIONAL;
  public const CONTINUATION_OPERATOR_INDEX = 2;
  public const CONTINUATION_VALUE_INDEX = 3;

  // > comment
  private const COMMENT = '(>)([^\\n]*)';
  public const COMMENT_OPERATOR_INDEX = 4;
  public const COMMENT_VALUE_INDEX = 5;

  // - value
  private const ITEM = '(-)(?!-)[^\\S\\n]*'.self::OPTIONAL;
  public const ITEM_OPERATOR_INDEX = 6;
  public const ITEM_VALUE_INDEX = 7;

  // -- key
  private const EMBED = '(--++)[^\\S\\n]*'.self::REQUIRED;
  public const EMBED_OPERATOR_INDEX = 8;
  public const EMBED_KEY_INDEX = 9;

  // #
  private const SECTION_OPERATOR = '(#++)';
  public const SECTION_OPERATOR_INDEX = 10;

  // # key
  private const SECTION_KEY_UNESCAPED = '([^\\s`<][^<\\n]*?)';
  public const SECTION_KEY_UNESCAPED_INDEX = 11;

  // # `key`
  public const SECTION_KEY_ESCAPE_BEGIN_OPERATOR_INDEX = 12;
  private const SECTION_KEY_ESCAPED = '(`++)[^\\S\\n]*(\\S[^\\n]*?)[^\\S\\n]*(\\'.self::SECTION_KEY_ESCAPE_BEGIN_OPERATOR_INDEX.')'; // TODO: Should this exclude the backreference inside the quotes? (as in ((?:(?!\1).)+) ) here and elsewhere (probably not because it's not greedy.?!
  public const SECTION_KEY_ESCAPED_INDEX = 13;
  public const SECTION_KEY_ESCAPE_END_OPERATOR_INDEX = 14;

  // # key < template
  // # `key` < template
  private const SECTION_KEY = '(?:'.self::SECTION_KEY_UNESCAPED.'|'.self::SECTION_KEY_ESCAPED.')';
  private const SECTION_TEMPLATE = '(?:(<(?!<)|<<)[^\\S\\n]*'.self::REQUIRED.')?';
  private const SECTION = self::SECTION_OPERATOR.'\\s*'.self::SECTION_KEY.'[^\\S\\n]*'.self::SECTION_TEMPLATE;
  public const SECTION_COPY_OPERATOR_INDEX = 15;
  public const SECTION_TEMPLATE_INDEX = 16;

  private const EARLY_DETERMINED = self::CONTINUATION.'|'.self::COMMENT.'|'.self::ITEM.'|'.self::EMBED.'|'.self::SECTION;

  // key:
  // key: value
  private const KEY_UNESCAPED = '([^\\s>#\\-`\\\\|:=<][^\\n:=<]*?)';
  public const KEY_UNESCAPED_INDEX = 17;

  // key:
  // `key`: value
  public const KEY_ESCAPE_BEGIN_OPERATOR_INDEX = 18;
  private const KEY_ESCAPED = '(`++)[^\\S\\n]*(\\S[^\\n]*?)[^\\S\\n]*(\\'.self::KEY_ESCAPE_BEGIN_OPERATOR_INDEX.')';
  public const KEY_ESCAPED_INDEX = 19;
  public const KEY_ESCAPE_END_OPERATOR_INDEX = 20;

  private const KEY = '(?:'.self::KEY_UNESCAPED.'|'.self::KEY_ESCAPED.')';

  private const EMPTY_OR_FIELD = '(:)[^\\S\\n]*'.self::OPTIONAL;
  public const FIELD_OPERATOR_INDEX = 21;
  public const FIELD_VALUE_INDEX = 22;

  // key =
  // `key` = value
  private const ENTRY = '(=)[^\\S\\n]*'.self::OPTIONAL;
  public const ENTRY_OPERATOR_INDEX = 23;
  public const ENTRY_VALUE_INDEX = 24;

  // key < template
  // `key` < template
  private const TEMPLATE = '(<(?!<)|<<)\\s*'.self::REQUIRED;
  public const COPY_OPERATOR_INDEX = 25;
  public const TEMPLATE_INDEX = 26;

  private const LATE_DETERMINED = self::KEY.'\\s*(?:'.self::EMPTY_OR_FIELD.'|'.self::ENTRY.'|'.self::TEMPLATE.')';

  private const NON_EMPTY_LINE = '(?:'.self::EARLY_DETERMINED.'|'.self::LATE_DETERMINED.')';

  public const LINE = '/[^\\S\\n]*(?:'.self::EMPTY_LINE.'|'.self::NON_EMPTY_LINE.')[^\\S\\n]*(?=\\n|$)/';
}
