<?php declare(strict_types=1);

namespace Enolib;
use \stdClass;
use Enolib\Error;

/**
 * Secondary parsing module - if there are copies, they get resolved here.
 * 
 * @internal
 */
class Resolver {
    private stdClass $context;
    private bool $index_non_section_elements;
    private bool $index_sections;
    
    public function __construct(stdClass $context) {
        $this->context = $context;
        $this->index_non_section_elements = $context->unresolved_non_section_elements;
        $this->index_sections = $context->unresolved_sections;
    }
    
    private function consolidateNonSectionInstructions(stdClass $instruction, stdClass $template) : void {
        if (array_key_exists('comment_instructions', $template) &&
            !array_key_exists('comment_instructions', $instruction)) {
            $instruction->comment_instructions = $template->comment_instructions;
        }

        if ($instruction->type === 'ELEMENT') {
            switch ($template->type) {
                case 'EMBED':
                $instruction->type = 'FIELD';
                $this->mirror($instruction, $template);
                break;
                case 'FIELD':
                $instruction->type = 'FIELD';
                $this->mirror($instruction, $template);
                break;
                case 'FIELDSET':
                $instruction->type = 'FIELDSET';
                $this->copyGeneric($instruction, $template);
                break;
                case 'LIST':
                $instruction->type = 'LIST';
                $this->copyGeneric($instruction, $template);
                break;
            }
        } else if($instruction->type === 'FIELDSET') {
            if($template->type === 'FIELDSET') {
            $this->mergeFieldsets($instruction, $template);
            } else if($template->type === 'FIELD' ||
            $template->type === 'LIST' ||
            $template->type === 'MULTILINE_FIELD_BEGIN') {
            $fieldset_entry_instruction = null;
            foreach($this->context->instructions as $candidate_instruction) {
            if($candidate_instruction->line > $instruction->line &&
            $candidate_instruction-> type === 'FIELDSET_ENTRY') {
            $fieldset_entry_instruction = $candidate_instruction;
            break;
            }
            }

            throw Parsing::missingFieldsetForFieldsetEntry($this->context, $fieldset_entry_instruction);
            }
        } else if($instruction->type === 'LIST') {
            if($template->type === 'LIST') {
            $this->copyGeneric($instruction, $template);
            } else if($template->type === 'FIELD' ||
            $template->type === 'FIELDSET' ||
            $template->type === 'MULTILINE_FIELD_BEGIN') {
            $list_item_instruction = null;
            foreach($this->context->instructions as $candidate_instruction) {
            if($candidate_instruction->line > $instruction->line &&
            $candidate_instruction-> type === 'LIST_ITEM') {
            $list_item_instruction = $candidate_instruction;
            break;
            }
            }

            throw Parsing::missingListForListItem($this->context, $list_item_instruction);
            }
        }

        $instruction->resolved = true;
    }
    
    /**
     * Iterates through the passed section's elements (and recurses deeper down), adding those that need to be copied to a global index.
     * 
     * Note that this does not do any copying yet, it just creates an index for the next step.
     */
    private function index_section(stdClass $section) : void {
        foreach ($section->elements as $instruction) {
            if ($instruction->type === 'SECTION') {
                $this->index($instruction);
                
                if ($this->index_sections &&
                    array_key_exists($instruction->key, $this->context->section_copy_ops_by_key) &&
                    $instruction->key !== $instruction->template) {
                    $copy_data = $this->context->section_copy_ops_by_key[$instruction->key];
                        
                    if (array_key_exists('template', $copy_data))
                        throw Error::twoOrMoreTemplatesFound($this->context, $copy_data->targets[0], $copy_data->template, $instruction);
                        
                    $copy_data->template = $instruction;
                }
            } else if ($this->index_non_section_elements &&
                       array_key_exists($instruction->key, $this->context->non_section_copy_ops_by_key) &&
                       $instruction->key !== $instruction->template) {
               $copy_data = $this->context->non_section_copy_ops_by_key[$instruction->key];
                   
               if (array_key_exists('template', $copy_data))
                   throw Error::twoOrMoreTemplatesFound($this->context, $copy_data->targets[0], $copy_data->template, $instruction);
                   
               $copy_data->template = $instruction;
            }
        }
    }
    
    /**
     * Performs the actual resolution - single and only method called from outside.
     */
    public function resolve() : void {
        if (!$this->context->index_non_section_elements &&
            !$this->context->index_sections)
            return;
        
        $this->index_section($this->context->document_instruction);
        
        while ($this->context->unresolved_non_section_instructions) {
            $this->resolveNonSectionElement($this->context->unresolved_non_section_instructions[0]);
        }

        while ($this->context->unresolved_section_instructions) {
            $this->resolveSection($this->context->unresolved_section_instructions[0]);
        }
    }
    
    private function resolveNonSectionElement(stdClass $instruction, array $previous_instructions = []) : void {
      if (in_array($instruction, $previous_instructions)) {
        throw Error::cyclicDependency($this->context, $instruction, $previous_instructions);
      }

      if (!array_key_exists($instruction->template, $this->context->non_section_template_index)) {
        throw Error::nonSectionElementNotFound($this->context, $instruction);
      }

      $templates = $this->context->non_section_template_index[$instruction->template];

      if (count($templates) > 1) {
        throw Error::multipleNonSectionElementTemplatesFound($this->context, $instruction, $templates);
      }

      $template = $templates[0];

      if (array_key_exists('template', $template) && !array_key_exists('resolved', $template)) {
        $this->resolveNonSectionElement($template, array_merge($previous_instructions, [$instruction]));
      }

      $this->consolidateNonSectionInstructions($instruction, $template);

      $index_to_remove = array_search($instruction, $this->context->unresolved_non_section_instructions);
      unset($this->context->unresolved_non_section_instructions[$index_to_remove]);
    }
}

// TODO: That's for pre-section resolution
if(array_key_exists('template', $instruction)) {
  $this->context->unresolved_section_instructions[] = $instruction;

  $instruction->resolve = true;
  for($section_index = 1; $section_index < count($this->active_section_instructions); $section_index++) {
    $this->active_section_instructions[$section_index]->deep_resolve = true;
  }
}