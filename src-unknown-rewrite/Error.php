<?php declare(strict_types=1);

// TODO: Slim down to an "enum"-type error identification only

namespace Enolib;
use Exception;

/**
 * Custom parsing error representation, which offers a rich set of information on the error.
 */
class Error extends Exception {
  public $cursor;
  public $message;
  public $selection;
  public $snippet;
  public $text;

  public function __construct(string $text, string $snippet, array $selection) {
    $this->cursor = $selection[0];
    $this->message = $text . "\n\n" . $snippet;
    $this->selection = $selection;
    $this->snippet = $snippet;
    $this->text = $text;

    parent::__construct($this->message, 0);
  }
}


// TODO: Error code enum?
// TODO: Which metadata do they all have? (What property changes on Error need to be made?)
// CYCLIC_DEPENDENCY Element copied into itself
// INVALID_LINE Line syntax does not follow any specified pattern
// MISSING_ELEMENT_FOR_CONTINUATION Continuation without any prior continuable element
// ENTRY_WITHOUT_FIELD Entry without containing field
// ITEM_WITHOUT_FIELD Item without containing field
// NON_SECTION_ELEMENT_NOT_FOUND Copying a non-existent non-section element 
// SECTION_HIERARCHY_LAYER_SKIP 