<?php declare(strict_types=1);

namespace Enolib;
use \OutOfRangeException;
use Enolib\{Analyzer,Messages,Resolver,Section,Tokenizer};

/**
 * Main entry point for parsing - call parse() on this.
 */
class Parser {
  public static function parse(string $input) : Section {
    $context = (object) [ 'input' => $input ];

    (new Analyzer($context))->analyze();
    (new Resolver($context))->resolve();

    $context->document = new Section($context, $context->document_instruction);

    return $context->document;
  }
}
