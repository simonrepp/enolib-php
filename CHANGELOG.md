# Changelog

## 0.1.2 / `2019-01-02`

#### Fixed

- Port core pattern matching fixes `7c07438`
- Ensure strict type checking for all comparison operations `b8206e6`

#### Maintenance

- Port core pattern matching spec suite `991b01e`
- Modularize grammar constants as a class `8980e35`

## 0.1.1 / `2018-11-08`

#### Fixed

- Ensure all requires are made relative from calling file `16f86e0`
- Remove erroneous dev context in installation instructions `2b54151`

## 0.1.0 / `2018-11-08`

Initial release
